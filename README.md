# Argus: retrieve Azure application meta-data based on application IDs.

This tool can be used for retrieving Azure application meta-data based on the application's ID. This is done by authenticating against a global administrator on a special-purpose O365 Azure AD, and faking a consent request.

Some example application UUIDs for testing:
* **Apple Internet Accounts**: f8d98a96-0999-43f5-8af3-69971c7bb423
* **Samsung E-mail**: 8acd33ea-7197-4a96-bc33-d7cc7101262f

## Dependencies
* Python 3
* Poetry
* Selenium

Applications:
* Google Chrome browser
* Chromedriver, matching the Chrome browser's version. Can be retrieved from [here](https://chromedriver.chromium.org/downloads).

## How to use: standalone
The application's help function provides the documentation:
```
$ python argus.py -h
usage: argus.py [-h] [-c CHROMEDRIVER_PATH] [--headless] -a APP_ID -u USERNAME [-p PASSWORD]

Argus: retrieve Azure application meta-data based on application IDs.

optional arguments:
  -h, --help            show this help message and exit
  -c CHROMEDRIVER_PATH, --chromedriver_path CHROMEDRIVER_PATH
                        relative or absolute path to the Chromedriver binary (default: ./chromedriver)
  --headless            run Chrome browser in headless mode (default: disabled)

required named arguments:
  -a APP_ID, --app_id APP_ID
                        ID (type: UUID) of the application to retrieve meta-data for
  -u USERNAME, --username USERNAME
                        username to be used (must have global administrator rights!)
  -p PASSWORD, --password PASSWORD
                        password to be used with the specified username (or prompt if not provided)
```

Example output:
```
$ python argus.py -a f8d98a96-0999-43f5-8af3-69971c7bb423 -u argus@eyeargus.onmicrosoft.com
Password:
{'Date created': datetime.datetime(2016, 10, 4, 0, 0),
 'Domain': 'apple.com',
 'Name': 'Apple Internet Accounts',
 'Publisher': 'Apple Inc.  Verified',
 'Reply URLs': 'https://apple.com',
 'whois': {'creation_date': datetime.datetime(1987, 2, 19, 5, 0),
           'dnssec': False,
           'expiration_date': datetime.datetime(2022, 2, 20, 5, 0),
           'last_updated': datetime.datetime(2021, 2, 16, 6, 26, 20),
           'name': 'apple.com',
           'name_servers': {'a.ns.apple.com',
                            'a.ns.apple.com\r',
                            'b.ns.apple.com',
                            'b.ns.apple.com\r',
                            'c.ns.apple.com',
                            'c.ns.apple.com\r',
                            'd.ns.apple.com',
                            'd.ns.apple.com\r'},
           'registrant': 'Apple Inc.',
           'registrant_country': 'US',
           'registrar': 'CSC Corporate Domains, Inc.',
           'status': 'clientTransferProhibited '
                     'https://icann.org/epp#clientTransferProhibited',
           'statuses': ['serverUpdateProhibited '
                        'http://www.icann.org/epp#serverUpdateProhibited',
                        'serverUpdateProhibited '
                        'https://icann.org/epp#serverUpdateProhibited',
                        'clientTransferProhibited '
                        'http://www.icann.org/epp#clientTransferProhibited',
                        'serverTransferProhibited '
                        'http://www.icann.org/epp#serverTransferProhibited',
                        'serverDeleteProhibited '
                        'https://icann.org/epp#serverDeleteProhibited',
                        'clientTransferProhibited '
                        'https://icann.org/epp#clientTransferProhibited',
                        'serverTransferProhibited '
                        'https://icann.org/epp#serverTransferProhibited',
                        'serverDeleteProhibited '
                        'http://www.icann.org/epp#serverDeleteProhibited']}}
```

## How to use: Docker
The application has been 'Dockerized' for use in K8 clusters, for example. All files related to the use with Docker are stored within the `docker` folder. The easiest way to use Argus using Docker is by means of the provided helper scripts:

```
$ ./docker_build.sh # required the Linux x86_64 Chromedriver binary to be stored in the project's main folder
$ ./docker_run.sh

Bash wrapper around the Argus utility for use with Docker. The provided
command-line arguments are copied as is to the Argus invocation call.

Syntax:  ./docker_run.sh <argus_arguments>
Example: ./docker_run.sh -a f8d98a96-0999-43f5-8af3-69971c7bb423 -u argus@eyeargus.onmicrosoft.com -p <password>

Note that the following arguments arguments are required:
      -a <application_id>
      -u <username>
      -p <password>

$ ./docker_run.sh -a f8d98a96-0999-43f5-8af3-69971c7bb423 -u argus@eyeargus.onmicrosoft.com -p <password>
{'Date created': datetime.datetime(2016, 4, 10, 0, 0),
 'Domain': 'apple.com',
 'Name': 'Apple Internet Accounts',
 'Publisher': 'Apple Inc.  Verified',
 'Reply URLs': 'https://apple.com',
 'whois': {'creation_date': datetime.datetime(1987, 2, 19, 5, 0),
           'dnssec': False,
           'expiration_date': datetime.datetime(2022, 2, 20, 5, 0),
           'last_updated': datetime.datetime(2021, 2, 16, 6, 26, 20),
           'name': 'apple.com',
           'name_servers': {'a.ns.apple.com',
                            'b.ns.apple.com',
                            'c.ns.apple.com',
                            'd.ns.apple.com'},
           'registrant': 'Apple Inc.',
           'registrant_country': 'US',
           'registrar': 'CSC Corporate Domains, Inc.',
           'status': 'clientTransferProhibited '
                     'https://icann.org/epp#clientTransferProhibited',
           'statuses': ['serverDeleteProhibited '
                        'http://www.icann.org/epp#serverDeleteProhibited',
                        'serverUpdateProhibited '
                        'http://www.icann.org/epp#serverUpdateProhibited',
                        'serverUpdateProhibited '
                        'https://icann.org/epp#serverUpdateProhibited',
                        'clientTransferProhibited '
                        'https://icann.org/epp#clientTransferProhibited',
                        'serverDeleteProhibited '
                        'https://icann.org/epp#serverDeleteProhibited',
                        'serverTransferProhibited '
                        'http://www.icann.org/epp#serverTransferProhibited',
                        'serverTransferProhibited '
                        'https://icann.org/epp#serverTransferProhibited',
                        'clientTransferProhibited '
                        'http://www.icann.org/epp#clientTransferProhibited']}}
```

## Further instructions
See the Makefile for all possible operations in this repository. Initialize your virtualenv with ```make init```. Run ```make all``` before committing changes to format and sort.
