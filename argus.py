import argparse
import getpass
import logging
from datetime import datetime
from os import path
from pprint import pprint
from sys import exit

import whois
from selenium.webdriver.chrome.options import Options

import const
from argus_driver import ArgusDriver


class Password:
    DEFAULT = "prompt if not specified"

    def __init__(self, value):
        if value == self.DEFAULT:
            value = getpass.getpass("Password: ")
        self.value = value

    def __str__(self):
        return self.value


def enrich_app_info(app_info):
    """
    Parse dates and enrich domain names with whois information.
    """
    if "Date created" in app_info:
        try:
            app_info["Date created"] = datetime.strptime(
                app_info["Date created"], "%d/%m/%Y"
            )
        except ValueError:
            # Ignore result if parsing failed
            logging.warning('Failed to parse date "' + app_info["Date created"] + '"')
            pass

    if "Domain" in app_info:
        try:
            app_info["whois"] = whois.query(app_info["Domain"]).__dict__
        except whois.exceptions.UnknownTld:
            # Ignore WHOIS result in case of an unknown TLD
            logging.warning('Failed to query WHOIS for "' + app_info["Domain"] + '"')
            pass


def fatal(msg):
    logging.error(msg)
    driver.quit()
    exit(1)


def init():
    arg_parser = argparse.ArgumentParser(
        description="Argus: retrieve Azure application meta-data based on application IDs."
    )
    arg_parser.add_argument(
        "-c",
        "--chromedriver_path",
        dest="chromedriver_path",
        type=str,
        help="relative or absolute path to the Chromedriver binary (default: "
        + const.CHROMEDRIVER_PATH
        + ")",
        default=const.CHROMEDRIVER_PATH,
    )
    arg_parser.add_argument(
        "--headless",
        dest="headless",
        action="store_true",
        help="run Chrome browser in headless mode (default: disabled)",
    )
    required_named = arg_parser.add_argument_group("required named arguments")
    required_named.add_argument(
        "-a",
        "--app_id",
        dest="app_id",
        type=str,
        required=True,
        help="ID (type: UUID) of the application to retrieve meta-data for",
    )
    required_named.add_argument(
        "-u",
        "--username",
        dest="username",
        type=str,
        required=True,
        help="username to be used (must have global administrator rights!)",
    )
    required_named.add_argument(
        "-p",
        "--password",
        dest="password",
        type=Password,
        help="password to be used with the specified username (or prompt if not provided)",
        default=Password.DEFAULT,
    )
    args = arg_parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s %(levelname)-5s [%(filename)s:%(lineno)d] %(message)s",
        level=logging.INFO,
    )

    options = Options()
    options.headless = args.headless
    options.add_argument("--window-size=1920,1200")
    options.add_argument("--lang=en-GB")  # crucial for comparing strings

    if not path.exists(args.chromedriver_path):
        logging.error(
            "Chromedriver could not be found (path: '" + args.chromedriver_path + "')"
        )
        exit(1)
    return (ArgusDriver(args.chromedriver_path, options), args)


def main():
    driver.get(const.AUTH_PATH + args.app_id)

    page_title = driver.get_page_title()
    if page_title == "Pick an account":
        fatal("Multi-account support is not implemented yet")
    elif page_title != "Sign in":
        fatal('Encountered unexpected page: "' + page_title + '"')

    if not driver.sign_in(args.username, str(args.password)):
        driver.quit()
        exit(1)
    if not driver.wait_page_title_changed(
        "Permissions requested\nReview for your organisation"
    ):
        page_title = driver.get_page_title()
        fatal(
            'Unexpected page (title: "'
            + page_title
            + '"); you probably provided an invalid application ID'
        )
    driver.open_app_info_modal()
    app_info = driver.extract_app_info()
    enrich_app_info(app_info)
    pprint(app_info)
    driver.quit()


if __name__ == "__main__":
    driver, args = init()
    main()
