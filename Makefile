.PHONY: all init format sort

CMD:=poetry run

BLACK_BIN=black
ISORT_BIN=isort

all: format sort

init:
	poetry install

format:
	$(CMD) $(BLACK_BIN) *.py

sort:
	$(CMD) $(ISORT_BIN) *.py
