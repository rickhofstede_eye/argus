#!/bin/bash

Help()
{
    # Display Help
    echo
    echo "Bash wrapper around the Argus utility for use with Docker. The provided"
    echo "command-line arguments are copied as is to the Argus invocation call."
    echo
    echo "Syntax:  ./docker_run.sh <argus_arguments>"
    echo "Example: ./docker_run.sh -a f8d98a96-0999-43f5-8af3-69971c7bb423 -u argus@eyeargus.onmicrosoft.com -p <password>"
    echo
    echo "Note that the following arguments arguments are required:"
    echo "      -a <application_id>"
    echo "      -u <username>"
    echo "      -p <password>"
    echo
}

if [ $# -eq 0 ]; then
    Help
    exit 1
fi

docker run argus "$@"
RETURN_CODE=$?
if [ ${RETURN_CODE} -ne 0 ]; then
    echo "WARNING: you probably forgot to provide a password (-p). This is required, as interactive sessions are not supported."
    exit ${RETURN_CODE}
fi
