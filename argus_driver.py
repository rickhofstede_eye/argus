import argparse
import logging
from time import sleep

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import (
    element_to_be_clickable,
    presence_of_element_located,
)
from selenium.webdriver.support.wait import WebDriverWait

import const


class ArgusDriver(webdriver.Chrome):
    def __init__(self, executable_path, options=None):
        super(ArgusDriver, self).__init__(
            executable_path=executable_path, options=options
        )

    def check_element_exists(self, xpath):
        try:
            return self.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return None

    def wait_element_clickable(self, xpath, timeout_sec=const.DRIVER_WAIT_TIME):
        self.wait_element_exists(xpath, timeout_sec)
        try:
            return WebDriverWait(self, timeout_sec).until(
                element_to_be_clickable((By.XPATH, xpath))
            )
        except TimeoutException:
            return None

    def wait_element_exists(self, xpath, timeout_sec=const.DRIVER_WAIT_TIME):
        try:
            return WebDriverWait(self, timeout_sec).until(
                presence_of_element_located((By.XPATH, xpath))
            )
        except TimeoutException:
            return None

    def get_page_title(self):
        header_element = self.wait_element_exists("//div[@role='heading']")
        title = header_element.get_attribute("innerText")
        return title

    def wait_page_title_changed(self, new_title="", max_retries=5):
        """
        Wait until the page title has changed, either to another value than the
        current one, or to a specified new one.

        Parameters:
            new_title (str):Title of the new page to wait for, or empty string
                if we just have to wait for the title to differ
                from the current one.
            max_retries(int):Maximum number of retries, with 1s waiting times.

        """
        current_title = self.get_page_title()
        if new_title == "":
            new_title = self.get_page_title()
        retries = 0
        while current_title != new_title and retries < max_retries:
            sleep(1)
            retries += 1
            current_title = self.get_page_title()
        return current_title == new_title

    def sign_in(self, username, password):
        username_element_xpath = "//input[@type='email']"
        username_element = self.wait_element_exists(username_element_xpath)
        if username_element is None:
            logging.error(
                'Element "' + username_element_xpath + '" expected but not found'
            )
            return False
        username_element.send_keys(username)

        submit_element_xpath = "//input[@type='submit'][@value='Next']"
        submit_element = self.wait_element_clickable(submit_element_xpath)
        if submit_element is None:
            logging.error(
                'Element "' + submit_element_xpath + '" expected but not found'
            )
            return False
        submit_element.click()

        # Wait for 'Sign in' button to become available
        submit_element_xpath = "//input[@type='submit'][@value='Sign in']"
        submit_element = self.wait_element_clickable(submit_element_xpath)
        if submit_element is None:
            logging.error(
                'Element "' + submit_element_xpath + '" expected but not found'
            )
            return False

        password_element_xpath = "//input[@type='password']"
        password_element = self.wait_element_exists(password_element_xpath)
        if password_element is None:
            logging.error(
                'Element "' + password_element_xpath + '" expected but not found'
            )
            return False
        password_element.send_keys(password)
        submit_element.click()

        self.wait_page_title_changed()
        new_page_title = self.get_page_title()
        if new_page_title == "Approve sign-in request":
            logging.error(
                '2FA is enabled for account "'
                + username
                + '"; please disable 2FA to use Argus'
            )
        elif new_page_title == "Enter password" and self.check_element_exists(
            "//div[@id='passwordError']"
        ):
            logging.error(
                'Incorrect credentials provided for account "' + username + '"'
            )
            return False
        return True

    def open_app_info_modal(self):
        button_element_xpath = "//a[@id='appDomainLinkToAppInfo']"
        button_element = self.check_element_exists(button_element_xpath)
        if button_element is None:
            logging.error(
                'Element "' + button_element_xpath + '" expected but not found'
            )
            return False

        button_element.click()

        modal_title_element_xpath = "//div[@id='AppInfoTitle']"
        modal_title_element = self.wait_element_exists(modal_title_element_xpath)
        if modal_title_element is None:
            logging.error(
                'Element "' + modal_title_element_xpath + '" expected but not found'
            )
            return False

    def extract_app_info(self):
        rows_xpath = "//div[@id='AppInfoContents']/div[@class='row']"
        rows = self.find_elements_by_xpath(rows_xpath)
        if len(rows) < 3:
            logging.error(
                'At least 3 elements expected for "'
                + rows_xpath
                + '" expected, but found '
                + str(len(rows))
            )
            return False

        app_info = {}
        for i in range(len(rows)):
            label_element = self.find_element_by_xpath(
                rows_xpath + "[" + str(i + 1) + "]/div[@class='label']"
            )
            value_element = self.find_element_by_xpath(
                rows_xpath + "[" + str(i + 1) + "]/div[@class='appInfoValue']"
            )
            app_info[
                label_element.get_attribute("innerText")
            ] = value_element.get_attribute("innerText")

        return app_info
